# Pasticceria Web

Pasticceria Frontend Webapp

## Test

È possibile testare il frontend lanciando il comando `REACT_APP_SERVICES_BASE_URL=<api-url> npm start`

## Author

- Nico Dante <[info@nicodante.it](mailto:info@nicodante.it)>
