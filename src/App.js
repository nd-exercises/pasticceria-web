import React, { Component } from "react";

import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";

import Shop from "./routes/shop/Shop";
import Admin from "./routes/admin/Admin";

import "./App.css";

class App extends Component {
  render() {
    return (
      <React.Fragment>
        <Router>
          <Switch>
            <Route path="/admin">
              <Admin />
            </Route>
            <Route path="/">
              <Shop />
            </Route>
          </Switch>
        </Router>
      </React.Fragment >
    );
  };
}

export default App;
