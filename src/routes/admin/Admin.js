import React, { Component } from "react";

import {
    Divider, List, ListItem, ListItemText, Link, CssBaseline, AppBar, Toolbar, IconButton, Typography, Drawer
} from "@material-ui/core";
import {
    ChevronLeft as ChevronLeftIcon,
    Menu as MenuIcon
} from '@material-ui/icons';
import { withStyles } from "@material-ui/core/styles";

import LogoIcon from "../../utils/LogoIcon";

import { instanceOf } from "prop-types";
import { withCookies, Cookies, CookiesProvider } from "react-cookie";
import { Route, Switch } from "react-router-dom";
import clsx from "clsx";

import PasticceriaHeader from "../../utils/PasticceriaHeader";

import { Constants } from "../../utils/Common";

import Login from "./Login";
import Cakes from "./Cakes";
import Recipes from "./Recipes";
import Users from "./Users";

const drawerWidth = 240;

const styles = theme => ({
    root: {
        display: "flex",
    },
    appBar: {
        transition: theme.transitions.create(["margin", "width"], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
    },
    appBarShift: {
        width: `calc(100% - ${drawerWidth}px)`,
        marginLeft: drawerWidth,
        transition: theme.transitions.create(["margin", "width"], {
            easing: theme.transitions.easing.easeOut,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    hide: {
        display: "none",
    },
    drawer: {
        width: drawerWidth,
        flexShrink: 0,
    },
    drawerPaper: {
        width: drawerWidth,
    },
    drawerHeader: {
        display: "flex",
        alignItems: "center",
        padding: theme.spacing(0, 1),
        // necessary for content to be below app bar
        ...theme.mixins.toolbar,
        justifyContent: "flex-end",
    },
    content: {
        flexGrow: 1,
        padding: theme.spacing(3),
        transition: theme.transitions.create("margin", {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
        marginLeft: -drawerWidth,
    },
    contentShift: {
        transition: theme.transitions.create("margin", {
            easing: theme.transitions.easing.easeOut,
            duration: theme.transitions.duration.enteringScreen,
        }),
        marginLeft: 0,
    },
});

class Admin extends Component {
    static propTypes = {
        cookies: instanceOf(Cookies).isRequired
    };

    state = {
        open: false
    };

    componentDidMount() {
        const { cookies } = this.props;

        const isLogged = () => {
            return new Promise((resolve, reject) => {
                const token = cookies.get(Constants.LOGIN_TOKEN_COOKIE_NAME);

                if (token && token.length > 0) {
                    fetch(Constants.SERVICES_BASE_URL + "/api/auth/verify/", {
                        method: "POST",
                        headers: {
                            "Content-Type": "application/json",
                        },
                        body: JSON.stringify({
                            token: token
                        })
                    }).then(res => res.json()).then((data) => {
                        if (data.user) {
                            cookies.set(Constants.USER_COOKIE_NAME, data.user, { path: "/" });

                            resolve(true);
                        } else {
                            resolve(false);
                        }
                    }).catch(err => {
                        console.log(err);

                        resolve(false);
                    });
                } else {
                    resolve(false);
                }
            });
        };

        const doLogout = () => {
            cookies.remove(Constants.USER_COOKIE_NAME, { path: "/" });
            cookies.remove(Constants.LOGIN_TOKEN_COOKIE_NAME, { path: "/" });

            window.location.href = "/admin";
        };

        isLogged().then(logged => {
            if (!logged && window.location.pathname !== "/admin/login") {
                window.location.href = "/admin/login";
            } else if (logged) {
                if (window.location.pathname === "/admin/login"
                    || window.location.pathname === "/admin") {
                    window.location.href = "/admin/cake";
                } else if (window.location.pathname === "/admin/logout") {
                    doLogout();
                }
            }
        }).catch(console.log);
    };

    render() {
        const _self = this;

        const handleDrawerOpen = () => {
            _self.setState({ open: true });
        };

        const handleDrawerClose = () => {
            _self.setState({ open: false });
        };

        return (
            <CookiesProvider>
                <PasticceriaHeader showAdminLink={false} />
                <main>
                    <Switch>
                        <Route path={"/admin/login"}>
                            <Login />
                        </Route>
                        <Route path={"/admin"}>
                            <div className={"root"}>
                                <CssBaseline />
                                <AppBar
                                    position="fixed"
                                    className={clsx(styles.appBar, {
                                        [styles.appBarShift]: this.state.open,
                                    })}
                                >
                                    <Toolbar>
                                        <IconButton
                                            color="inherit"
                                            aria-label="open drawer"
                                            onClick={handleDrawerOpen}
                                            edge="start"
                                            className={clsx(styles.menuButton, this.state.open && styles.hide)}
                                        >
                                            <MenuIcon />
                                        </IconButton>
                                        <LogoIcon className="icon" width="24px" title="Pasticceria web" />
                                        <Typography variant="h6" color="inherit" noWrap>
                                            <Link href="/" color="inherit">Pasticceria Online</Link>
                                        </Typography>
                                    </Toolbar>
                                </AppBar>
                                <Drawer
                                    className={styles.drawer}
                                    variant="persistent"
                                    anchor="left"
                                    open={this.state.open}
                                    styles={{
                                        paper: styles.drawerPaper,
                                    }}
                                >
                                    <div className={styles.drawerHeader}>
                                        <IconButton onClick={handleDrawerClose}>
                                            <ChevronLeftIcon />
                                        </IconButton>
                                    </div>
                                    <Divider />
                                    <List>
                                        <ListItem button key={"torte"}>
                                            <ListItemText>
                                                <Link href="/admin/cake" color="inherit">Torte</Link>
                                            </ListItemText>
                                        </ListItem>
                                        <ListItem button key={"ricette"}>
                                            <ListItemText>
                                                <Link href="/admin/recipe" color="inherit">Ricette</Link>
                                            </ListItemText>
                                        </ListItem>
                                    </List>
                                    <Divider />
                                    <List>
                                        <ListItem button key={"utenti"}>
                                            <ListItemText>
                                                <Link href="/admin/user" color="inherit">Utenti</Link>
                                            </ListItemText>
                                        </ListItem>
                                    </List>
                                    <Divider />
                                    <List>
                                        <ListItem button key={"logout"}>
                                            <ListItemText>
                                                <Link href="/admin/logout" color="inherit">Logout</Link>
                                            </ListItemText>
                                        </ListItem>
                                    </List>
                                </Drawer>
                                <div className={styles.content} style={{ paddingTop: "50px" }}>
                                    <Switch>
                                        <Route path={"/admin/cake"}>
                                            <Cakes />
                                        </Route>
                                        <Route path={"/admin/recipe"}>
                                            <Recipes />
                                        </Route>
                                        <Route path={"/admin/user"}>
                                            <Users />
                                        </Route>
                                    </Switch>
                                </div>
                            </div>
                        </Route>
                    </Switch>
                </main>
            </CookiesProvider>
        )
    };
}

export default withCookies(withStyles(styles)(Admin));