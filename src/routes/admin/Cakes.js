import React, { useState } from "react";

import {
    Container, Grid, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Paper
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import moment from "moment";

const useStyles = makeStyles({
    table: {
        width: "100%"
    }
});

const Cakes = () => {
    const classes = useStyles();

    const [cakes, setCakes] = useState(false);

    setCakes([]);

    const calcSalePrice = (basePrice, productionDate, saleDate) => {
        if (!saleDate) {
            return "";
        }

        return "5€";
    };

    return (
        <Container maxWidth="md">
            <Grid container spacing={4}>
                <TableContainer component={Paper}>
                    <Table className={classes.table} aria-label="simple table">
                        <TableHead>
                            <TableRow>
                                <TableCell>Ricetta</TableCell>
                                <TableCell align="right">Data di produzione</TableCell>
                                <TableCell align="right">Prezzo di base</TableCell>
                                <TableCell align="right">Data di vendita</TableCell>
                                <TableCell align="right">Prezzo di vendita</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {cakes.map(cake => (
                                <TableRow key={cake.id}>
                                    <TableCell component="th" scope="row">
                                        {cake.recipe.name}
                                    </TableCell>
                                    <TableCell align="right">{moment(cake.productionDate).format("DD/MM/YYYY")}</TableCell>
                                    <TableCell align="right">{cake.recipe.basePrice}</TableCell>
                                    <TableCell align="right">{moment(cake.saleDate).format("DD/MM/YYYY")}</TableCell>
                                    <TableCell align="right">{calcSalePrice(cake.recipe.basePrice, cake.productionDate, cake.saleDate)}</TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
            </Grid>
        </Container>
    )
};

export default Cakes;