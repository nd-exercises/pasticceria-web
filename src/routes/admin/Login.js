import React, { useState } from "react";
import clsx from "clsx";

import {
    Container, Grid, TextField, FormControl, InputLabel, OutlinedInput, InputAdornment, IconButton,
    Button
} from "@material-ui/core";
import { Visibility, VisibilityOff } from "@material-ui/icons";
import { makeStyles } from "@material-ui/core/styles";

import { useCookies } from "react-cookie";
import { Constants } from "../../utils/Common";

import "./Login.css";

const useStyles = makeStyles(theme => ({
    root: {
        display: "flex",
        flexWrap: "wrap",
    },
    margin: {
        margin: theme.spacing(1),
    },
    withoutLabel: {
        marginTop: theme.spacing(3),
    },
    textField: {
        width: "35ch",
    },
    button: {
        width: "35ch",
    }
}));

const Login = () => {
    const classes = useStyles();

    const [values, setValues] = useState({
        email: "",
        password: "",
        showPassword: false,
    });

    const [cookies, setCookie] = useCookies(Constants.LOGIN_TOKEN_COOKIE_NAME);

    const handleChange = prop => event => {
        setValues({ ...values, [prop]: event.target.value });
    };

    const handleClickShowPassword = () => {
        setValues({ ...values, showPassword: !values.showPassword });
    };

    const handleMouseDownPassword = event => {
        event.preventDefault();
    };

    const handleSubmit = event => {
        event.preventDefault();

        fetch(Constants.SERVICES_BASE_URL + "/api/auth/login/", {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify({
                email: values.email,
                password: values.password
            })
        }).then(res => {
            if (res.status === 200) {
                return res.json();
            }

            if (res.status === 401) {
                alert("utente non autorizzato");
                return;
            }

            alert("errore inaspettato");
        }).then(data => {
            console.log("token: " + data.token);

            setCookie(Constants.LOGIN_TOKEN_COOKIE_NAME, data.token, { path: '/' });

            console.log("cookie token: " + cookies[Constants.LOGIN_TOKEN_COOKIE_NAME]);

            window.location.href = "/admin";
        }).catch(console.log);
    };

    return (
        <form className={classes.root} noValidate onSubmit={handleSubmit}>
            <Container className="cardGrid" maxWidth="xs">
                <Grid container spacing={4}>
                    <FormControl className={clsx(classes.margin, classes.textField)} variant="outlined">
                        <TextField
                            id="login-email-text"
                            label="Email"
                            variant="outlined"
                            value={values.email}
                            onChange={handleChange("email")}
                        />
                    </FormControl>
                    <FormControl className={clsx(classes.margin, classes.textField)} variant="outlined">
                        <InputLabel htmlFor="login-password-text">Password</InputLabel>
                        <OutlinedInput
                            id="login-password-text"
                            type={values.showPassword ? "text" : "password"}
                            value={values.password}
                            onChange={handleChange("password")}
                            endAdornment={
                                <InputAdornment position="end">
                                    <IconButton
                                        aria-label="toggle password visibility"
                                        onClick={handleClickShowPassword}
                                        onMouseDown={handleMouseDownPassword}
                                        edge="end"
                                    >
                                        {values.showPassword ? <Visibility /> : <VisibilityOff />}
                                    </IconButton>
                                </InputAdornment>
                            }
                            labelWidth={70}
                        />
                    </FormControl>
                    <FormControl className={clsx(classes.margin, classes.button)}>
                        <Button
                            type="submit"
                            variant="contained"
                            color="primary"
                            disableElevation
                            disabled={
                                !values.email || values.email.length === 0
                                || !values.password || values.password.length === 0
                            }
                        >
                            Accedi
                        </Button>
                    </FormControl>
                </Grid>
            </Container>
        </form >
    )
};

export default Login;