import React, { Component } from "react";

import {
    Container, Grid, Typography, Card, CardContent, CardActions, Button, Dialog, DialogActions,
    DialogContent, DialogContentText, DialogTitle, Paper, List, ListItem, ListItemText
} from "@material-ui/core";
import Draggable from 'react-draggable';
import Moment from 'react-moment';

import PasticceriaHeader from "../../utils/PasticceriaHeader";

import { confirm } from "../../utils/ConfirmDialog";

import { Constants } from "../../utils/Common";

import "./Shop.css"

const PaperComponent = (props) => (
    <Draggable handle="#draggable-dialog-title" cancel={'[class*="MuiDialogContent-root"]'}>
        <Paper {...props} />
    </Draggable>
);

class Shop extends Component {
    state = {
        cakes: [],
        openRecipeDialog: false,
        recipe: {
            name: "",
            description: "",
            ingredients: []
        }
    };

    componentDidMount() {
        fetch(Constants.SERVICES_BASE_URL + "/api/shop/cake/").then(res => res.json()).then((data) => {
            this.setState({ cakes: data });
        }).catch(console.log);
    };

    render() {
        const _self = this;

        const openRecipe = (cakeId) => {
            fetch(Constants.SERVICES_BASE_URL + "/api/shop/cake/" + cakeId + "/").then(res => res.json()).then((data) => {
                this.setState({
                    openRecipeDialog: true,
                    recipe: data.recipe
                });
            }).catch(console.log);
        };

        const closeRecipe = () => {
            _self.setState({ openRecipeDialog: false });
        };

        const buyCake = (cakeId) => {
            confirm("Sicuro di volerla comprare?").then(isConfirmed => {
                if (isConfirmed) {
                    return fetch(Constants.SERVICES_BASE_URL + "/api/shop/cake/" + cakeId + "/set-sale-date/", {
                        method: "POST",
                        headers: {
                            "Content-Type": "application/json",
                        },
                        body: JSON.stringify({
                            saleDate: new Date()
                        })
                    });
                }

                return Promise.resolve(null);
            }).then(res => fetch(Constants.SERVICES_BASE_URL + "/api/shop/cake/")).then(res => res.json()).then((data) => {
                _self.setState({ cakes: data });
            }).catch(console.log);
        };

        return (
            <React.Fragment>
                <PasticceriaHeader showAdminLink={true} />
                <main>
                    <Container className="cardGrid" maxWidth="md">
                        <Typography component="h1" variant="h2" align="center" color="textPrimary" gutterBottom>
                            Le nostre torte
                    </Typography>
                        <Grid container spacing={4}>
                            {this.state.cakes.map(cakeItem => (
                                <Grid item key={cakeItem.cakeId} xs={12} sm={6} md={4}>
                                    <Card className={"card"}>
                                        <CardContent className={"cardContent"}>
                                            <Typography gutterBottom variant="h5" component="h2">
                                                {cakeItem.recipeName}
                                            </Typography>
                                            <Typography variant="body2" component="p">
                                                <label>fatta il</label>: <Moment format="DD/MM/YYYY">{cakeItem.productionDate}</Moment>
                                                <br />
                                                <label>prezzo</label>: {cakeItem.price}
                                            </Typography>
                                        </CardContent>
                                        <CardActions>
                                            <Button size="small" color="primary" onClick={(e) => { openRecipe(cakeItem.cakeId) }}>
                                                Ricetta
                                        </Button>
                                            <Button size="small" color="primary" onClick={(e) => { buyCake(cakeItem.cakeId) }}>
                                                Compra
                                        </Button>
                                        </CardActions>
                                    </Card>
                                </Grid>
                            ))}
                        </Grid>
                        <Dialog
                            open={this.state.openRecipeDialog}
                            onClose={closeRecipe}
                            PaperComponent={PaperComponent}
                            aria-labelledby="draggable-dialog-title"
                        >
                            <DialogTitle style={{ cursor: 'move' }} id="draggable-dialog-title" className={"recipeDialogTitle"}>
                                {this.state.recipe.name}
                            </DialogTitle>
                            <DialogContent>
                                <DialogContentText>
                                    {this.state.recipe.description || "nessuna ricetta"}
                                </DialogContentText>
                                <b>Ingredienti:</b>
                                <List dense={false}>
                                    {this.state.recipe.ingredients.map(ingredient => (
                                        <ListItem key={ingredient.id}>
                                            <ListItemText>
                                                {ingredient.name} {ingredient.quantity} {ingredient.uom}
                                            </ListItemText>
                                        </ListItem>
                                    ))}
                                </List>
                            </DialogContent>
                            <DialogActions>
                                <Button autoFocus onClick={closeRecipe} color="primary">
                                    Chiudi
                            </Button>
                            </DialogActions>
                        </Dialog>
                    </Container>
                </main>
            </React.Fragment>
        )
    };
}

export default Shop;