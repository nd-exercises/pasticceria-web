export class Constants {
    static SERVICES_BASE_URL = process.env.REACT_APP_SERVICES_BASE_URL || "";

    static LOGIN_TOKEN_COOKIE_NAME = "login-token";
    static USER_COOKIE_NAME = "logged-in-user";
};