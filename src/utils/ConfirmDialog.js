import React from "react";

import { Button, Dialog, DialogTitle, DialogActions, DialogContent, DialogContentText, Paper } from "@material-ui/core";
import Draggable from "react-draggable";
import { confirmable, createConfirmation } from "react-confirm";

const PaperComponent = (props) => (
    <Draggable handle="#draggable-dialog-title" cancel={'[class*="MuiDialogContent-root"]'}>
        <Paper {...props} />
    </Draggable>
);

const ConfirmDialog = confirmable(({ show, proceed, confirmation, options }) => (
    <Dialog
        open={show}
        onClose={() => proceed(false)}
        PaperComponent={PaperComponent}
        aria-labelledby="draggable-dialog-title"
    >
        <DialogTitle style={{ cursor: "move", minWidth: "20vw" }} id="draggable-dialog-title">
            {"Conferma"}
        </DialogTitle>
        <DialogContent>
            <DialogContentText>
                {confirmation}
            </DialogContentText>
        </DialogContent>
        <DialogActions>
            <Button autoFocus onClick={() => proceed(true)} color="primary">
                Ok
            </Button>
            <Button autoFocus onClick={() => proceed(false)} color="primary">
                Annulla
            </Button>
        </DialogActions>
    </Dialog>
));

export function confirm(confirmation, options = {}) {
    return createConfirmation(ConfirmDialog)({ confirmation, options });
};
