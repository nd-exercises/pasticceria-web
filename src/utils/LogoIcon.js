import React from "react";

const LogoIcon = ({
    style = {},
    width = "100%",
    className = "",
    title = "Logo Icon"
}) => (
        <img src={process.env.PUBLIC_URL + "/logo192.png"} width={width} height={width} alt={title} style={style} className={className} />
    );

export default LogoIcon;