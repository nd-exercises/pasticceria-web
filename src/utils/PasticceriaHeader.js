import React from "react";

import { AppBar, Toolbar, Typography, Link } from "@material-ui/core";

import LogoIcon from "./LogoIcon";

import "./PasticceriaHeader.css";

const PasticceriaHeader = (props) => (
    <AppBar position="relative">
        <Toolbar>
            <LogoIcon className="icon" width="24px" title="Pasticceria web" />
            <Typography variant="h6" color="inherit" noWrap>
                <Link href="/" color="inherit">
                    Pasticceria Online
                </Link>
            </Typography>
            <Typography color="inherit" noWrap className={"admin-link"}>
                <Link href="/admin" color="inherit" style={{ display: props.showAdminLink ? "" : "none" }}>
                    Area riservata
                </Link>
            </Typography>
        </Toolbar>
    </AppBar >
);

export default PasticceriaHeader;